CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
The registration2sugar module allows contact information that is collected
via the registration module to be sent auttomatically to a SugarCRM instance.

 * To submit bug reports and feature suggestions, or to track changes:
  https://drupal.org/sandbox/jankeconsulting/2244027

REQUIREMENTS
------------
This module requires the following modules:
 * Entity Registrattion (https://drupal.org/project/registration)

INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
  https://drupal.org/documentation/install/modules-themes/modules-7
  for further information.

CONFIGURATION
-------------
In order for this module to work, the url for the SugarCRM instance must
be set. Optionally a campaign id can be defined.

Both values can be set at Administration >> Configuration >> 
System >> Registration2Sugar.

MAINTAINERS
-----------
Current maintainers:
 * Janke Consulting - https://drupal.org/user/2873165
 * Ralph Janke (txwikinger) - https://drupal.org/user/515140 
This project has been sponsored by:
 * JANKE CONSULTING
  Specialised in helping our customers to allow their software adapt to their
  business model. http://www.jankeconsulting.ca
 
