<?php

/**
 * @file
 * Administration forms for the registration2sugar module
 */

/**
 * Menu callback; Displays the administration settings for registration2sugar.
 */
function registration2sugar_admin_settings() {
  $form = array();

  $form['registration2sugar'] = array(
    '#type' => 'fieldset',
    '#title' => t('Registration2Sugar Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['registration2sugar']['registration2sugar_sugarcrmurl'] = array(
    '#type' => 'textfield',
    '#title' => t('SugarCRM URL'),
    '#default_value' => variable_get('registration2sugar_sugarcrmurl'),
    '#description' => t('Set the URL for the SugarCRM system.'),
    '#required' => TRUE,
  );

  $form['registration2sugar']['registration2sugar_campaign_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Campaign Id'),
    '#default_value' => variable_get('registration2sugar_campaign_id'),
    '#description' => t('Set the campaign id for the SugarCRM system.'),
  );

  return system_settings_form($form);

}
